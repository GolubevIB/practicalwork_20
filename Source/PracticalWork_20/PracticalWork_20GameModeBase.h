// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PracticalWork_20GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PRACTICALWORK_20_API APracticalWork_20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
